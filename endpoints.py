base_url = "http://18.184.234.77:8080"

#Ping Controller
ping = f'{base_url}/ping'

#Calculator Controller
calculator_add = f"{base_url}/add"

#Accounts Controller
accounts = f"{base_url}/accounts"
accounts_create = f"{base_url}/accounts/create"
accounts_delete = f"{base_url}/accounts/delete"
accounts_pay = f"{base_url}/accounts/pay"
accounts_withdraw = f"{base_url}/accounts/withdraw"
