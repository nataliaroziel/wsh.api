import requests
from faker import Faker

import endpoints

fake = Faker()

class Account:
    def __init__(self):
        self.name = fake.uuid4()

    def create(self):
        body = { "name": self.name}
        create_account_response = requests.put(endpoints.accounts_create, json=body)
        assert create_account_response.status_code == 201

    def delete(self):
        account_params = {"account": self.name}
        delete_response = requests.delete(endpoints.accounts_delete, params = account_params)
        assert delete_response.status_code == 200

    def pay(self, amount):
        account_params = {"account": self.name}
        body = {"amount": amount}
        pay_response = requests.post(endpoints.accounts_pay, json = body, params = account_params)
        assert pay_response.status_code == 200

    def withdraw(self,amount):
        account_params = {"account": self.name}
        body = {"amount": amount}
        withdraw_response = requests.post(endpoints.accounts_withdraw, json=body, params=account_params)
        assert withdraw_response.status_code == 200

    def get_balance(self):
        new_account = {"account": self.name}
        filtered_list_response = requests.get(endpoints.accounts, params=new_account)
        return filtered_list_response.json()["accounts"][0]["balance"]["accountBalance"]

if __name__ == "__main__":
    account = Account()
    print(account.name)
    account.create()
    account.delete()

