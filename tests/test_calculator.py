import random

import requests

import endpoints


def test_add_calculator():
    first_number, second_number = random.randint(1,1000), random.randint(1,1000)
    expected_result = first_number + second_number

    response = requests.post(endpoints.calculator_add)
    body = {
        "firstNumber": first_number,
        "secondNumber": second_number
    }
    create_add_calculator_response = requests.post(endpoints.calculator_add, json = body)
    assert create_add_calculator_response.status_code == 200
    result_dict = create_add_calculator_response.json()
    actual_result = result_dict["result"]
    assert actual_result == expected_result
    expected_result = first_number + second_number


    print(first_number)
    print (second_number)
    print (expected_result)
















