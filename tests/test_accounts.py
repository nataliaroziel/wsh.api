import pytest
import requests
from faker import Faker

import endpoints
# medota testowa, która pobiera dane wszystkich kont
from models.account import Account

fake = Faker()

def test_get_accounts_list(new_account):

    response = requests.get(endpoints.accounts)
    assert response.status_code == 200
    response_dict = response.json()
    accounts_lists = response_dict["accounts"]
    name_lists = [ a ["name"] for a in accounts_lists]
    print(name_lists)
    assert new_account.name in name_lists

def test_create_account(new_account):
    list_params = {"account" : new_account.name}
    filtered_list_response = requests.get(endpoints.accounts, params = list_params)
    assert filtered_list_response.status_code == 200
    assert new_account.name in filtered_list_response.text

@pytest.fixture
def new_account():
    account = Account()
    account.create()
    return account

def test_delete_account(new_account):
    new_account.delete()
    account_params = {"account": new_account.name}
    filtered_list_response = requests.get(endpoints.accounts, params = account_params)
    assert filtered_list_response.status_code == 404

def test_balance_account(new_account):
    assert new_account.get_balance() == 1000

def test_whole_account(new_account):
    new_account.pay(200)
    assert new_account.get_balance() == 1200
    new_account.withdraw(133)
    assert new_account.get_balance() == 1067















